import azure.functions as func
import datetime
import json
import logging

app = func.FunctionApp()


@app.route(route="MyHttpTriggerBluecat", auth_level=func.AuthLevel.ANONYMOUS)
def MyHttpTriggerBluecat(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    target_endpoint = "https://ipam.leidos.com/"  # Replace with your actual HTTPS endpoint

    try:
        # Use requests library for HTTP requests
        import requests

        response = requests.get(target_endpoint)
        response.raise_for_status()  # Raise an exception if the request fails

        output_message = f"Hey buddy, I sent a GET request to {target_endpoint} and received this response:\n\n{response.text}"
        return func.HttpResponse(
            output_message,
            status_code=200
        )
    except Exception as e:
        logging.error(f"Failed to trigger API endpoint: {e}")
        return func.HttpResponse(
            "Failed to hit the target endpoint.",
            status_code=500
        )

# # Initial default test
# @app.route(route="MyHttpTrigger", auth_level=func.AuthLevel.ANONYMOUS)
# def MyHttpTrigger(req: func.HttpRequest) -> func.HttpResponse:
#     logging.info('Python HTTP trigger function processed a request.')

#     name = req.params.get('name')
#     if not name:
#         try:
#             req_body = req.get_json()
#         except ValueError:
#             pass
#         else:
#             name = req_body.get('name')

#     if name:
#         return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
#     else:
#         return func.HttpResponse(
#              "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
#              status_code=200
#         )

