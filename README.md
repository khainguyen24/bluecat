# Bluecat
## Setup environment

#cd to the project folder
cd MyProjFolder

#create venv environment
python -m venv .venv

#start the environment
source ./.venv/bin/activate

## Install requirements.txt
pip install -r requirements.txt

## Start the functions runtime locally
func start

## Test out the HttpTrigger function in browser
http://localhost:7071/api/MyHttpTriggerBluecat